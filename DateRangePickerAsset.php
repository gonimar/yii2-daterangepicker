<?php
namespace yiicms\widgets;

use yii\web\AssetBundle;

class DateRangePickerAsset extends AssetBundle
{
    public $sourcePath = '@yiicms/widgets/assets';
    public $css = [
        'daterangepicker-bs3.css',
    ];
    public $js = [
        'moment.min.js',
        'daterangepicker.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}