~~~
<?php echo \yiicms\widgets\DatePicker::widget([
        'model' => $model,
        'attribute' => 'contract_date',
        'options' => ['class' => 'form-control'],
        'clientOptions' => [
            'startDate' => date('d/m/Y', time()),
            'endDate' => date('d/m/Y', time()),
            'minDate' => '01/01/2010',
            'maxDate' => date('d/m/Y', time() + 60 * 60 * 24 * 30),
            //'dateLimit' => array('days' => 60),
            'showDropdowns' => true,
            'showWeekNumbers' => false,
            'timePicker' => false,
            'timePickerIncrement' => 15,
            'timePicker12Hour' => false,
            'ranges' => [
                'Сегодня' => new \yii\web\JsExpression('[moment(), moment()]'),
                'Вчера' => new \yii\web\JsExpression('[moment().subtract("days", 1), moment().subtract("days", 1)]'),
                'Последние 7 дней' => new \yii\web\JsExpression('[moment().subtract("days", 6), moment()]'),
                'Последние 30 дней' => new \yii\web\JsExpression('[moment().subtract("days", 29), moment()]'),
                'Этот месяц' => new \yii\web\JsExpression('[moment().startOf("month"), moment().endOf("month")]'),
                'Прошлый месяц' => new \yii\web\JsExpression('[moment().subtract("month", 1).startOf("month"), moment().subtract("month", 1).endOf("month")]'),
            ],
            'opens' => 'right',
            'buttonClasses' => new \yii\web\JsExpression('["btn btn-default"]'),
            'applyClass' => 'btn-small btn-primary',
            'cancelClass' => 'btn-small',
            'format' => 'DD/MM/YYYY',
            'separator' => ' до ',
            'locale' => [
                'applyLabel' => 'Выбрать',
                'cancelLabel' => 'Отмена',
                'fromLabel' => 'с',
                'toLabel' => 'по',
                'customRangeLabel' => 'Свой период',
                'daysOfWeek' => ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
                'monthNames' => ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                'firstDay' => 1
            ]
        ],
    ]); ?>
~~~
